from rest_framework import serializers

from .models import GrantGoal

class GrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"