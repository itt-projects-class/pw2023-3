from django.urls import path

from api import views

app_name = "api"


urlpatterns = [
    ##### CLIENTS #####
    path('ws/grantgoal/list', views.wsListGrantGoal, name="ws_grantgoal"),
    path('client/grantgoal/create/', views.CreateGrantGoalClient.as_view(), name="client_grantgoal_create"),
    path('client/grantgoal/', views.ListGrantGoalClient.as_view(), name="client_grantgoal"),
    path('client/grantgoal/detail/<str:gname>/', views.DetailGrantGoalClient.as_view(), name="client_detail"),
    path('client/grantgoal/update/<int:pk>/', views.UpdateGrantGoalClient.as_view(), name="client_update"),

    ###### V2 API #####
    path('v3/create/grantgoal/', views.CreateGrantGoal.as_view(), name="create_grantgoal"),
    path('v2/list/grant_goals/', views.ListGrantGoal.as_view(), name="list_grantgoals"),
    path('v2/detail/grant_goal/<int:pk>/', views.DetailGrantGoal.as_view(), name="detail_grantgoal"),
    path('v3/update/grant_goal/<int:pk>/', views.UpdateGrantGoal.as_view(), name="update_grantgoal"),
]
