from django.shortcuts import render, HttpResponse, redirect
from django.core import serializers
from django.views import generic
import requests

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics

# Create your views here.
from .models import GrantGoal
from .forms import CreateGrantGoalForm
from .serializers import GrantGoalSerializer


def wsListGrantGoal(request):
    queryset = GrantGoal.objects.all()
    data = serializers.serialize('json', queryset)
    return HttpResponse(data, content_type='application/json')


### CREATE
class CreateGrantGoal(generics.CreateAPIView):
    serializer_class = GrantGoalSerializer
   


## RETRIEVE


class ListGrantGoal(APIView):
    def get(self, request, *args, **kwargs):
        grant_goals = GrantGoal.objects.filter(status=True)
        data = GrantGoalSerializer(grant_goals, many=True).data
        return Response(data)


class DetailGrantGoal(APIView):
    def get(self, request, pk, *args, **kwargs):
        grantgoal = GrantGoal.objects.get(pk=pk)
        data = GrantGoalSerializer(grantgoal).data
        return Response(data)



### UPDATE
class UpdateGrantGoal(generics.UpdateAPIView):
    serializer_class = GrantGoalSerializer
    queryset = GrantGoal.objects.all()

### DELETE



##### CLIENTS #####


### CREATE
class CreateGrantGoalClient(generic.View):
    template_name = "api/client_create_gg.html"
    context = {}
    url = "http://localhost:8000/api/v3/create/grantgoal/"
    form_class = CreateGrantGoalForm()
    response = ""

    def get(self, request):
        self.context = {
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request, *args, **kwargs):
        payload = {
            "goal_title": request.POST["goal_title"],
            "description": request.POST["description"],
            "days_duration": request.POST["days_duration"],  
            "status": request.POST["status"],
            "state": request.POST["state"],
            "sprint": request.POST["sprint"],
            "slug": request.POST["slug"],
            "user": request.POST["user"]
        }
        self.response = requests.post(url=self.url, data=payload)
        print(self.response)
        return redirect("/api/client/grantgoal")


### RETRIEVE
class ListGrantGoalClient(generic.View):
    template_name = "api/client.html"
    context = {}
    # url = "http://localhost:8000/api/v1/ws/grantgoal/list"
    url = "http://localhost:8000/api/v2/list/grant_goals/"
    def get(self, request):
        response = requests.get(url=self.url)
        #objects = response.json()
        self.context = {
            "list_grantgoal": response.json()
        }
        return render(request, self.template_name, self.context)
    


class DetailGrantGoalClient(generic.View):
    template_name = "api/client_detail.html"
    context = {}
    # url = "http://localhost:8000/api/v1/ws/grantgoal/list"
    url = "http://localhost:8000/api/v2/detail/grant_goal/"
    def get(self, request, gname, *args, **kwargs):
        self.url = self.url + f"{gname}/"
        print("-------",self.url)
        response = requests.get(url=self.url)
        self.context = {
            "grantgoal": response.json()
        }
        return render(request, self.template_name, self.context)


### UPDATE
class UpdateGrantGoalClient(generic.View):
    template_name = "api/client_update.html"
    context = {}
    url_get = "http://localhost:8000/api/v2/detail/grant_goal/"
    url_put = "http://localhost:8000/api/v3/update/grant_goal/"
    response = ""


    def get(self, request, pk):
        self.url_get = self.url_get + f"{pk}/"
        self.response = requests.get(url=self.url_get)
        self.context = {
            "grantgoal": self.response.json()
        }
        return render(request, self.template_name, self.context)

    def post(self, request, pk):
        self.url_put = self.url_put + f"{pk}/"
        payload = {            
            "user": request.POST["user"],
            "goal_title": request.POST["goal_title"],
            "description": request.POST["description"],
            "days_duration": request.POST["days_duration"],
            "status": request.POST["status"],
            "state": request.POST["state"],
            "sprint": request.POST["sprint"],
            "slug": request.POST["slug"]
        }
        print("------", self.url_put)
        self.response = requests.put(url=self.url_put, data=payload)
        return render(request, self.template_name, self.context)
### DELETE


    