from django.contrib import admin

# Register your models here.
from api import models


@admin.register(models.GrantGoal)
class GrantGoalAdmin(admin.ModelAdmin):
    list_display = [
        "goal_title",
        "user",
        "timestamp",
        "final_date",
        "days_duration"
    ]


@admin.register(models.Goal)
class GoalAdmin(admin.ModelAdmin):
    list_display = [
        "goal_name",
        "user",
        "timestamp",
        "final_date",
        "duration"
    ]


@admin.register(models.SubGoal)
class SubGoalAdmin(admin.ModelAdmin):
    list_display = [
        "subgoal_name",
        "user",
        "timestamp",
        "duration"
    ]


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = [
        "task_name",
        "user",
        "timestamp",
        "duration"
    ]