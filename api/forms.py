from django import forms


from .models import GrantGoal

class CreateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "user",
            "goal_title",
            "description",
            "days_duration",
            "state",
            "sprint",
            "status",
            "slug"
        ]
        widgets = {
            "user": forms.Select(attrs={"class":"form-select", "placeholder": "Seleccione un User"}),
            "goal_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del Grant Goal"}),
            "description": forms.Textarea(attrs={"class":"form-control", "cols":"5", "type":"text"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "sprint": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control"})
        }