from django.forms.models import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic

from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.messages.views import SuccessMessageMixin

from api.models import GrantGoal, Goal
from .forms import GrantGoalForm, UpdateGrantGoalForm, LoginForm, UserForm
# Create your views here.

############## GRANT GOAL CRUD ##############

## List
class Index(generic.View):
    template_name = "home/index.html"
    context = {}
    form_class = LoginForm


    def get(self, request, *args, **kwargs):
        self.context = {
            "grant_goals": GrantGoal.objects.filter(status=True),
            "form": self.form_class 
        }
        return render(request, self.template_name, self.context)

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("home:index")
        else:
            return redirect("home:index")






## DEtail
class DetailGrantGoal(generic.View):
    template_name = "home/detail_grant_goal.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        self.context = {
            "grant_goal": GrantGoal.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)



##Create
class NewGrantGoal(SuccessMessageMixin, generic.CreateView):
    template_name = "home/new_grantgoal.html"
    model = GrantGoal
    form_class = GrantGoalForm
    success_url = reverse_lazy("home:new_grant_goal")
    success_message = "The New Grant Goal Was Created! ..."


    
    
## Update
class UpdateGrantGoal(SuccessMessageMixin, generic.UpdateView):
    template_name = "home/update_grantgoal.html"
    model = GrantGoal
    form_class = UpdateGrantGoalForm
    success_url = reverse_lazy("home:index")
    success_message = "The Grant Goal Was Updated! ..."
    


class DeleteGrantGoal(generic.DeleteView):
    template_name = "home/delete_grantgoal.html"
    model = GrantGoal
    success_url = reverse_lazy("home:index")


###################### GOAL'S CRUD ############################### 

## Retrieve: List
class ListGoal(generic.View):
    template_name = "home/list_goal.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "goals": Goal.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)


class DetailGoal(generic.DetailView):
    template_name = "home/detail_goal.html"
    model = Goal



class FullCalendarGG(generic.View):
    template_name = "fullcalendar/fullcalendar.html"
    context = {}

    def get(self, request):
        self.context = {
            "grant_goals": GrantGoal.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)



##### LOGIN LOGOUT SIGNUP #####

class Logout(generic.View):
    def get(self, request):
        logout(request)
        return render(request, "home/index.html", {})



class SignUp(generic.CreateView):
    template_name = "home/signup.html"
    model = User
    form_class = UserForm
    success_url = reverse_lazy("home:index")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get("username")
        password1 = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password1)
        if user is not None:
            login(self.request, user)
        return redirect("home:index")

# context = {
#     "pacientes": Citas.objects.filter(medico=request.user, turno=turno)
# }



# class Login(generic.View):
#     template_name = "home/index.html"
#     context = {}
#     form_class = LoginForm

#     def get(self, request):
#         self.context = {
#             "form": self.form_class
#         }
#         return render(request, self.template_name, self.context)