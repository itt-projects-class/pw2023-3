from django.urls import path

from . import views

app_name = "home"


urlpatterns = [
    
    path('new_grant_goal/', views.NewGrantGoal.as_view(), name="new_grant_goal"),
    path('update_grant_goal/<int:pk>/', views.UpdateGrantGoal.as_view(), name="update_grant_goal"),
    path('detail_grant_goal/<int:pk>/', views.DetailGrantGoal.as_view(), name="detail_grant_goal"),
    path('delete_grant_goal/<int:pk>/', views.DeleteGrantGoal.as_view(), name="delete_grant_goal"),
    #### GOAL'S URLS
    path('goal/list/', views.ListGoal.as_view(), name="list_goal"),
    path('goal/detail/<int:pk>/', views.DetailGoal.as_view(), name="detail_goal"),

    ### FULL CALENDAR URL'S
    path('fullcalendar/', views.FullCalendarGG.as_view(), name="fullcalendar"),

    #### AUTHENTICATE SYSTEM URLS ####
    path('logout/', views.Logout.as_view(), name="logout"),
    path('', views.Index.as_view(), name="index"),
    path('signup/', views.SignUp.as_view(), name="signup"),
]