from django import forms
from django.contrib.auth.forms import UserCreationForm

from api.models import GrantGoal

class UserForm(UserCreationForm):
    username = forms.CharField(max_length=16, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"})) 
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu Password"}))
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Confirma tu Password"}))  
    first_name = forms.CharField(max_length=16, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu First Name"})) 
    last_name = forms.CharField(max_length=16, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Last Name"})) 
    email = forms.EmailField(max_length=32, widget=forms.EmailInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Email"})) 


class GrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "user",
            "goal_title",
            "description",
            "days_duration",
            "state",
            "sprint",
            "slug"
        ]
        widgets = {
            "user": forms.Select(attrs={"class":"form-select", "placeholder": "Seleccione un User"}),
            "goal_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del Grant Goal"}),
            "description": forms.Textarea(attrs={"class":"form-control", "cols":"5", "type":"text"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "sprint": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control"})
        }


class UpdateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "user",
            "goal_title",
            "description",
            "final_date",
            "status",
            "slug"
        ]
        widgets = {
            "user": forms.Select(attrs={"class": "form-select"}),
            "goal_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Escribe el nombre del grant goal"}),
            "description": forms.Textarea(attrs={"class":"form-control", "cols":"5", "type":"text"}),
            "final_date": forms.DateInput(attrs={"type": "date", "class": "form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "sprint": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control"})
        }





class LoginForm(forms.Form):
    username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"}))
    password = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu Password"}))